%计算二维问题PPS,HSS最小迭代步数并输出对应实验最优参数以及CPU时间
%输入 系数矩阵A 位移参数alpha 初始向量x0 右端项f 最大迭代步数maxit 容忍误差tol
%输出 实际最优参数alpha_PPS 最小迭代步数IT_PPS 对应时间CPU_PPS 残差RES_PPS
%     实际最优参数alpha_HSS 最小迭代步数IT_HSS 对应时间CPU_HSS 残差RES_HSS
clear
load('data16.mat');
[n,n] = size(A);
x0=zeros(n,1);
maxit = 1000;
tol   = 1e-5;
ITCPU = zeros(100,8);
for ii=1:100
    alpha = 0.01*ii;
    ITCPU(ii,1) = alpha;
    [x,ITCPU(ii,2),ITCPU(ii,3),ITCPU(ii,4),flag] =  PPS_kro(A,alpha, x0, f, maxit, tol);
end
for ii=1:100
    alpha = 0.1*ii;
    ITCPU(ii,5) = alpha;
    [x,ITCPU(ii,6),ITCPU(ii,7),ITCPU(ii,8),flag] = HSS(A,alpha, x0, f, maxit, tol);
end
[Y,I]=min(ITCPU,[],1);
alpha_PPS=ITCPU(I(2),1)
IT_PPS=Y(2)
CPU_PPS=ITCPU(I(2),3)
RES_PPS=ITCPU(I(2),4)
alpha_HSS=ITCPU(I(6),5)
IT_HSS=Y(6)
CPU_HSS=ITCPU(I(6),7)
RES_HSS=ITCPU(I(6),8)


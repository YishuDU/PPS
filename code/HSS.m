function [x,iter,time,res,flag] = HSS(A,alpha, x0, b, maxit, tol)
%采用HSS分裂 求解线性方程组Ax=b
%输入 系数矩阵A 位移参数alpha 初始向量x0 右端项b 最大迭代步数maxit 容忍误差tol
%输出 解x 迭代步数iter CPU时间time 残差res 收敛标志flag: flag=0表示在maxit次迭代内收敛至所需容差tol 
flag = 0;
[n,n] = size(A);
P = alpha*speye(n,n);
H = (A+A')/2;
S = (A-A')/2;

IH = inv(P+H);
IS = inv(P+S);

b1 = IH*b;
b2 = IS*b;
x = x0;
bb=norm(b-A*x0);

t0 = clock;
for iter=1:maxit
    x = IH*(alpha*x-S*x)+b1;
    x = IS*(alpha*x-H*x)+b2;
    res=norm(b-A*x)/bb;
    if (res < tol)
        flag = 1;
        break;
    end
end
time = etime(clock,t0);
    
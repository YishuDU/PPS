clear
load('example3_data\data64.mat')
% [n,n] = size(A);
% x0=zeros(n,1);
% maxit = 1000;
% tol   = 1e-5;
% ITCPU = zeros(20,8);
% 
% I=eye(N);
% dplu_l=mean(diag(Dplus_l));
% dplu_r=mean(diag(Dplus_r));
% dmin_r=mean(diag(Dminus_r));
% dmin_l=mean(diag(Dminus_l));
% P1=1/2*yita*kron(M,M)+(dplu_l*(kron(I,T_l)+kron(T_l,I))-dplu_r*(kron(I,T_r)+kron(T_r,I)));
% P2=1/2*yita*kron(M,M)+(dmin_l*(kron(I,T_l')+kron(T_l',I))-dmin_r*(kron(I,T_r')+kron(T_r',I)));
% 
% P1s=full(preS(P1));
% P2s=full(preS(P2));
% ITCPU=[];
% % CCS预处理子
% for ii=1:500
%     alpha=ii*0.01;
%     ITCPU(ii,1)=alpha;
%     P1_ss=sqrt(1/(2*alpha))*(alpha*eye(N^2)+P1s);
%     P2_ss=sqrt(1/(2*alpha))*(alpha*eye(N^2)+P2s);
%     P_ss=P1_ss*P2_ss;
%     [~,it, ITCPU(ii,3), ITCPU(ii,4), ITCPU(ii,5)] = mygmres_FFT(A,zeros(N^2,1),f,P_ss,N^2,1000,1e-5 ); %GMRES(CCSF) 预处理子合 采用FFT
%     ITCPU(ii,2)=it(2);
% end
% [Y,I]=min(ITCPU(:,2),[],1);
% alpha_PPS=ITCPU(I,1)
% IT_PPS=Y
% CPU_PPS=ITCPU(I,3)
% RES_PPS=ITCPU(I,5)
% ITCPU(1,2)
% ITCPU(200,2)
% 
% % Strang Gilbert预处理子
% %Ps=yita*M_s+dplu_l*T_ls-dplu_r*T_rs+dmin_r*T_ls'-dmin_l*T_rs'; %为保持循环结构，取对角矩阵对角线元素为平均值
% Ps=full(preS(A));
% [~,iter5, time5, flag5, error5] = mygmres_FFT(A,zeros(N^2,1),f,Ps,N^2,1000,1e-5 ) %GMRES(SF) 采用FFT
% 
% % Tony Chan预处理子
% Pc=full(preC(A));
% [~,iter7, time7, flag7, error7] = mygmres_FFT(A,zeros(N^2,1),f,Pc,N^2,1000,1e-5 ) %GMRES(CF) 采用FFT

%无预处理子
[~,iter8, time8, flag8, error8] = mygmres(A,zeros(N^2,1),f,eye(N^2),N^2,1000,1e-5 ) %GMRES
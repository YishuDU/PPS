semilogy(0:iter1(2),resvec1(1:iter1(2)+1),'k-');hold on;
semilogy(0:iter2(2),resvec2(1:iter2(2)+1),'g-');hold on;
semilogy(0:iter3(2),resvec3(1:iter3(2)+1),'b-');hold on;
semilogy(0:iter4(2),resvec4(1:iter4(2)+1),'r-');hold on;

title('\beta=0.5')
xlabel('Number of iterations');
ylabel('Relative residual');
legend('GMRES(P)','GMRES(S)','GMRES(C)','GMRES');
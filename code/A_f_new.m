%生成一维分数阶扩散方程系数矩阵与源项并保存数据
clear 
global yita M Dplu_l Dplu_r Dmin_l Dmin_r T_l T_r N
i=9;   %矩阵维数2^i
N=2^i;
M_t=N;
beta=0.8;%扩散项分数阶导数2-beta
h=(1-0)/(N+1);
deltat=(1-0)/M_t;
tao=1/(gamma(beta+1)*h^(1-beta));
yita=h/(tao*deltat);
t=1/M_t;
% 生成第一个时间层上的系数矩阵A
x_1=h/2:h:1-3*h/2;
x_2=3*h/2:h:1-h/2;
Dplu_l=diag(Dplus(x_1,t));
Dplu_r=diag(Dplus(x_2,t));
Dmin_l=diag(Dminus(x_1,t));
Dmin_r=diag(Dminus(x_2,t));

q=zeros(1,N);
for k=0:N
    if (k==0)
        q_0=(1/2)^beta;
    else if (k==1)
        q(1)=(3/2)^beta-2*(1/2)^beta;
    else 
        q(k)=(k+1/2)^beta-2*(k-1/2)^beta+(k-3/2)^beta;
        end
    end
end
c_l=[q_0,q(1:N-1)];
r_l=[q_0,zeros(1,N-1)];
T_l=toeplitz(c_l,r_l);
c_r=q;
r_r=[q(1),q_0,zeros(1,N-2)];
T_r=toeplitz(c_r,r_r);
M=diag(3/4*ones(N,1))+diag(1/8*ones(N-1,1),1)+diag(1/8*ones(N-1,1),-1);
A=yita*M+Dplu_l*T_l-Dplu_r*T_r+Dmin_r*T_l'-Dmin_l*T_r';
K1=Dplu_l*T_l-Dplu_r*T_r;
K2=Dmin_r*T_l'-Dmin_l*T_r';
P1=1/2*yita*M+Dplu_l*T_l-Dplu_r*T_r;
P2=1/2*yita*M+Dmin_r*T_l'-Dmin_l*T_r';
% 生成第一个时间层上的源项f 
%example1 源项
y=@(x) exp(t)*x.^2.*(1-x).^2-(exp(t)*(0.01+t^2+10*exp(-8.*x)))/gamma(1+beta).*(2*x.^beta-12*x.^(beta+1)/(beta+1)+24*x.^(beta+2)/((beta+1)*(beta+2)))-...
    (exp(t)*(0.01+t^2+10*exp(8.*x)))/gamma(1+beta).*(2*(1-x).^beta-12*(1-x).^(beta+1)/(beta+1)+24*(1-x).^(beta+2)/((beta+1)*(beta+2)))-...
    (-80*exp(t)*exp(-8*x))/gamma(1+beta).*(2*x.^(beta+1)/(beta+1)-12*x.^(beta+2)/((beta+1)*(beta+2))+24*x.^(beta+3)/((beta+1)*(beta+2)*(beta+3)))-...
    (80*exp(t)*exp(8*x))/gamma(1+beta).*(-2*(1-x).^(beta+1)/(beta+1)+12*(1-x).^(beta+2)/((beta+1)*(beta+2))-24*(1-x).^(beta+3)/((beta+1)*(beta+2)*(beta+3)));

% %example2 源项
% y=@(x) exp(t)*x.^2.*(1-x).^2-(exp(t)*(0.01+t^2+0.1*exp(8.*x)))/gamma(1+beta).*(2*x.^beta-12*x.^(beta+1)/(beta+1)+24*x.^(beta+2)/((beta+1)*(beta+2)))-...
%     (exp(t)*(0.01+t^2+0.1*exp(-8.*x)))/gamma(1+beta).*(2*(1-x).^beta-12*(1-x).^(beta+1)/(beta+1)+24*(1-x).^(beta+2)/((beta+1)*(beta+2)))-...
%     (0.8*exp(t)*exp(8*x))/gamma(1+beta).*(2*x.^(beta+1)/(beta+1)-12*x.^(beta+2)/((beta+1)*(beta+2))+24*x.^(beta+3)/((beta+1)*(beta+2)*(beta+3)))-...
%     (-0.8*exp(t)*exp(-8*x))/gamma(1+beta).*(-2*(1-x).^(beta+1)/(beta+1)+12*(1-x).^(beta+2)/((beta+1)*(beta+2))-24*(1-x).^(beta+3)/((beta+1)*(beta+2)*(beta+3)));
f=zeros(1,N);
x_3=h/2:h:1-h/2;
for k=1:N
    f(k)=quad(y,x_3(k),x_3(k+1));
end
f=gamma(beta+1)*h^(1-beta)*f';
% 左除计算第一个时间层上的数值解
% x_4=h:h:1-h;
% b=yita*M*(x_4.^2.*(1-x_4).^2)'+f';
% xx=A\b;
eval(['save data',num2str(i)])%保存数据.mat文件

%example1 扩散系数
function d=Dplus(x,t)%取递减函数
    d=0.01+t^2+10*exp(-8*x);
end

function d=Dminus(x,t)%取递增函数 
    d=0.01+t^2+10*exp(8*x);
end

% %example2 扩散系数
% function d=Dplus(x,t)%改为递增函数
%     d=0.01+t^2+0.1*exp(8*x);
% end
% 
% function d=Dminus(x,t)%改为递减函数
%     d=0.01+t^2+0.1*exp(-8*x);
% end

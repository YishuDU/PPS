function Pc = preC(T)
% ����Tony Chanѭ���������Toeplitz����T
% ����Toeplitz����T
% ���ѭ������Pc

[N,N]=size(T);
t0=T(1,1);
t=T(2:N,1);
tt=T(1,2:N);
c0=t0;
c=zeros(N-1,1);
for i=1:N-1
    c(i)=((N-i)*t(i)+i*tt(N-i))/N;
end
col=[c0;c];
row=[c0;flipud(c)]';
Pc=toeplitz(col,row);
end
clear
load('example1_data_new\data12_beta07.mat')

dplu_l=mean(diag(Dplu_l));
dplu_r=mean(diag(Dplu_r));
dmin_r=mean(diag(Dmin_r));
dmin_l=mean(diag(Dmin_l));
T_ls=preS(T_l);
T_rs=preS(T_r);
M_s=preS(M);
I=eye(N);
ITCPU=[];
%% CCS预处理子

alpha=9.4;

P1_ss=sqrt(1/(2*alpha))*(alpha*I+1/2*yita*M_s+dplu_l*T_ls-dplu_r*T_rs);
P2_ss=sqrt(1/(2*alpha))*(alpha*I+1/2*yita*M_s+dmin_r*T_ls'-dmin_l*T_rs');
P_ss=P1_ss*P2_ss;

%[~,iter2, time2, flag2, error2] = mygmres(A,zeros(N,1),f,P_ss,N,1000,1e-5 ) %GMRES(CCS)

[~,iter1, CPU1, flag1, res1, resvec1] = mygmres_FFT(A,zeros(N,1),f,P_ss,N,1000,1e-5 ); %GMRES(CCSF) 预处理子合 采用FFT

% [~,iter3, time3, flag3, error3] = mygmres_FFT_CCS(A,zeros(N,1),f,P1_ss,P2_ss,N,1000,1e-5 ) %GMRES(CCSF) 预处理子分开 采用FFT



%% Strang Gilbert预处理子
Ps=yita*M_s+dplu_l*T_ls-dplu_r*T_rs+dmin_r*T_ls'-dmin_l*T_rs'; %为保持循环结构，取对角矩阵对角线元素为平均值
% Ps=yita*M+Dplu_l*T_ls-Dplu_r*T_rs+Dmin_r*T_ls'-Dmin_l*T_rs'; %为模拟真实扩散系数，取扩散系数为对角矩阵，虽然预处理子不为循环矩阵，但数值结果表明这类预处理子CPU时间更小
%[~,iter4, time4, flag4, error4] = mygmres(A,zeros(N,1),f,Ps,N,1000,1e-5 ) %GMRES(S)
[~,iter2, CPU2, flag2, res2, resvec2] = mygmres_FFT(A,zeros(N,1),f,Ps,N,1000,1e-5 ) %GMRES(SF) 采用FFT

%% Tony Chan预处理子
T_lc=preC(T_l);
T_rc=preC(T_r);
M_c=preC(M);
Pc=yita*M_c+dplu_l*T_lc-dplu_r*T_rc+dmin_r*T_lc'-dmin_l*T_rc'; %为保持循环结构，取对角矩阵对角线元素为平均值
%Pc=yita*M+Dplu_l*T_lc-Dplu_r*T_rc+Dmin_r*T_lc'-Dmin_l*T_rc';  %为模拟真实扩散系数，取扩散系数为对角矩阵，虽然预处理子不为循环矩阵，但数值结果表明这类预处理子CPU时间更小
%[~,iter6, time6, flag6, error6] = mygmres(A,zeros(N,1),f,Pc,N,1000,1e-5 ) %GMRES(C)
[~,iter3, CPU3, flag3, res3, resvec3] = mygmres_FFT(A,zeros(N,1),f,Pc,N,1000,1e-5 ) %GMRES(CF) 采用FFT

%% 无预处理子
% tic
% [~,iter4, CPU4, flag4, res4, resvec4] = mygmres_FFT(A,zeros(N,1),f,I,N,1000,1e-5 ) %GMRES(CF) 采用FFT
% time=toc
 [~,iter4, CPU4, flag4, res4, resvec4] = mygmres(A,zeros(N,1),f,eye(N),N,1000,1e-5 ) %GMRES


%[~, error, iter, flag, time] = mymygmres( A, zeros(N,1), f, eye(N), N, N, 1e-5 )

% tic
% [x,flag,relres,iter,~] = gmres(A,f,N,1e-5,N)
% time=toc



% [~,flag,iter1,CPU,res(i,4),resvec1] = PCG_FFT(A,f,tol,maxit,P2l,P2u,x0);
% 
% Ps=preS(A);                   
% [~,flag,iter2,time,res,resvec2] = PCG_FFT_one(A,f,1e-6,3000,Ps,zeros(N,1))%Strang
% 
% Pc=preC(A); 
% [~,flag,iter3,time,res,resvec3] = PCG_FFT_one(A,f,1e-6,3000,Pc,zeros(N,1))%T.chan
% 
% [~,iter4,time,res,flag,resvec4] = mycg(A,f,zeros(N,1),3000,1e-6)%无预处理
%%
semilogy(0:iter1(2),resvec1(1:iter1(2)+1),'k-');hold on;
semilogy(0:iter2(2),resvec2(1:iter2(2)+1),'g-');hold on;
semilogy(0:iter3(2),resvec3(1:iter3(2)+1),'b-');hold on;
semilogy(0:iter4(2),resvec4(1:iter4(2)+1),'r-');hold on;

title('\beta=0.5')
xlabel('Number of iterations');
ylabel('Relative residual');
legend('GMRES(P)','GMRES(S)','GMRES(C)','GMRES');
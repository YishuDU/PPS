%比较四种预处理子计算效率，分别为：GMRES(PPS)、GMRES(CCS)、GMRES(S)、GMRES(C)
%及FFT加速效果 GMRES(CCSF)、GMRES(SF)、GMRES(CF)
%及无预处理子 GMRES

clear
%load('C:\Users\duyis\Desktop\PPS程序\example1_data\data12_1_5.mat')

load('C:\Users\duyis\Desktop\PPS程序-20190905y邮箱下载\example1_data\data121_5.mat')

dplu_l=mean(diag(Dplu_l));
dplu_r=mean(diag(Dplu_r));
dmin_r=mean(diag(Dmin_r));
dmin_l=mean(diag(Dmin_l));
T_ls=preS(T_l);
T_rs=preS(T_r);
M_s=preS(M);

%% PPS预处理子
alpha=0.013;%需调
P1=1/2*yita*M+Dplu_l*T_l-Dplu_r*T_r;
P2=1/2*yita*M+Dmin_r*T_l'-Dmin_l*T_r';
I=eye(N);
P=(1/(2*alpha))*(alpha*I+P1)*(alpha*I+P2); 
[~,iter1, time1, flag1, error1] = mygmres(A,zeros(N,1),f,P,N,1000,1e-5 ) %GMRES(PPS)

%% CCS预处理子
P1_ss=sqrt(1/(2*alpha))*(alpha*I+1/2*yita*M_s+dplu_l*T_ls-dplu_r*T_rs);
P2_ss=sqrt(1/(2*alpha))*(alpha*I+1/2*yita*M_s+dmin_r*T_ls'-dmin_l*T_rs');
P_ss=P1_ss*P2_ss;

[~,iter2, time2, flag2, error2] = mygmres(A,zeros(N,1),f,P_ss,N,1000,1e-5 ) %GMRES(CCS)

[~,iter5, time5, flag5, error5] = mygmres_FFT(A,zeros(N,1),f,P_ss,N,1000,1e-5 ) %GMRES(SF) 采用FFT
% [~,iter3, time3, flag3, error3] = mygmres_FFT_CCS(A,zeros(N,1),f,P1_ss,P2_ss,N,1000,1e-5 ) %GMRES(CCSF) 采用FFT


%% Strang Gilbert预处理子
Ps=yita*M_s+dplu_l*T_ls-dplu_r*T_rs+dmin_r*T_ls'-dmin_l*T_rs'; %为保持循环结构，取对角矩阵对角线元素为平均值
% Ps=yita*M+Dplu_l*T_ls-Dplu_r*T_rs+Dmin_r*T_ls'-Dmin_l*T_rs'; %为模拟真实扩散系数，取扩散系数为对角矩阵，虽然预处理子不为循环矩阵，但数值结果表明这类预处理子CPU时间更小
[~,iter4, time4, flag4, error4] = mygmres(A,zeros(N,1),f,Ps,N,1000,1e-5 ) %GMRES(S)
[~,iter5, time5, flag5, error5] = mygmres_FFT(A,zeros(N,1),f,Ps,N,1000,1e-5 ) %GMRES(SF) 采用FFT

%% Tony Chan预处理子
T_lc=preC(T_l);
T_rc=preC(T_r);
M_c=preC(M);
Pc=yita*M_c+dplu_l*T_lc-dplu_r*T_rc+dmin_r*T_lc'-dmin_l*T_rc'; %为保持循环结构，取对角矩阵对角线元素为平均值
%Pc=yita*M+Dplu_l*T_lc-Dplu_r*T_rc+Dmin_r*T_lc'-Dmin_l*T_rc';  %为模拟真实扩散系数，取扩散系数为对角矩阵，虽然预处理子不为循环矩阵，但数值结果表明这类预处理子CPU时间更小
[~,iter6, time6, flag6, error6] = mygmres(A,zeros(N,1),f,Pc,N,1000,1e-5 ) %GMRES(C)
[~,iter7, time7, flag7, error7] = mygmres_FFT(A,zeros(N,1),f,Pc,N,1000,1e-5 ) %GMRES(CF) 采用FFT

%% 无预处理子
[~,iter8, time8, flag8, error8] = mygmres(A,zeros(N,1),f,eye(N),N,1000,1e-5 ) %GMRES
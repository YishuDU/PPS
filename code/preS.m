function Ps = preS(T)
% ����Strang Gilbertѭ���������Toeplitz����T
% ����Toeplitz����T
% ���ѭ������Ps

[N,N]=size(T);
if mod(N,2)==1% ����
    m=(N-1)/2;
    t0=T(1,1);
    s0=t0;
    t=T(2:m+1,1);
    tt=T(1,2:m+1);
    col=[s0;t;flipud(tt')];
    row=[s0,tt,fliplr(t')];
    Ps=toeplitz(col,row);   
else % ż��
    m=N/2;
    t0=T(1,1);
    s0=t0;
    t=T(2:m,1);
    tt=T(1,2:m);
    col=[s0;t;0;flipud(tt')];
    row=[s0,tt,0,fliplr(t')];
    Ps=toeplitz(col,row);  
end
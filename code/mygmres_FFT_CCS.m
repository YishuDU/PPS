function [x, it, time, flag, error] = mygmres_FFT_CCS( A, x, b, M1, M2, restrt, maxit, tol )
  %采用FFT加速向量左除循环矩阵 其中M1和M2为循环矩阵
  %采用右预处理GMRES 求解线性方程组Ax=b
  %输入 系数矩阵A 右端项b 容忍误差tol 最大迭代步数maxit 预处理子 M = M1 * M2  初始向量x 
  %输出 解x 收敛标志flag: flag=0表示在maxit次迭代内收敛至所需容差tol 残差res 迭代步数iter CPU时间time 

   iter = 0;                                       
   flag = 0;

   bnrm2 = norm( b );
   if  ( bnrm2 == 0.0 )
       bnrm2 = 1.0; 
   end

    %r = M \ ( b-A*x );
    r =  b-A*x ;
   error = norm( r ) / bnrm2;
   if ( error < tol ) 
       return
   end
   
   [n,n] = size(A);                                 
   m = restrt;
   V(1:n,1:m+1) = zeros(n,m+1);
   H(1:m+1,1:m) = zeros(m+1,m);
   cs(1:m) = zeros(m,1);
   sn(1:m) = zeros(m,1);
   e1    = zeros(n,1);
   e1(1) = 1.0;
   tic
   for iter = 1:maxit                             
      %r = M \ ( b-A*x );
      r =  b-A*x ;
      V(:,1) = r / norm( r );
      s = norm( r )*e1;
      for iter2 = 1:m                                  
            %w = M \ (A*V(:,iter2));                        
            %w = A*(M\V(:,iter2));
            %w = A*(M2\M1\V(:,iter2));
            v1=M1(:,1);   %用FFT
            xx1=fft(V(:,iter2));
            vv1=fft(v1).^(-1);
            z1=vv1.*xx1;
            %w1=ifft(z1);
            
            v2=M2(:,1);   %用FFT
            %xx2=fft(w1);
            xx2=z1;
            vv2=fft(v2).^(-1);
            z2=vv2.*xx2;
            w=A*ifft(z2);
            for k = 1:iter2
                H(k,iter2)= w'*V(:,k);
                w = w - H(k,iter2)*V(:,k);
            end
            H(iter2+1,iter2) = norm( w );
            V(:,iter2+1) = w / H(iter2+1,iter2);
            for k = 1:iter2-1                              
                temp     =  cs(k)*H(k,iter2) + sn(k)*H(k+1,iter2);
                H(k+1,iter2) = -sn(k)*H(k,iter2) + cs(k)*H(k+1,iter2);
                H(k,iter2)   = temp;
            end
            [cs(iter2),sn(iter2)] = rotmat( H(iter2,iter2), H(iter2+1,iter2) );
            temp   = cs(iter2)*s(iter2);                       
            s(iter2+1) = -sn(iter2)*s(iter2);
            s(iter2)   = temp;
            H(iter2,iter2) = cs(iter2)*H(iter2,iter2) + sn(iter2)*H(iter2+1,iter2);
            H(iter2+1,iter2) = 0.0;
            error  = abs(s(iter2+1)) / bnrm2;
            if ( error <= tol )                        
                y = H(1:iter2,1:iter2) \ s(1:iter2);               
                %x = x + V(:,1:iter2)*y;
                %x = x + M\(V(:,1:iter2)*y);
                %x = x + M2\M1\(V(:,1:iter2)*y);
                v1=M1(:,1);   %用FFT
                xx1=fft((V(:,1:iter2)*y));
                vv1=fft(v1).^(-1);
                z1=vv1.*xx1; 
        
                v2=M2(:,1);   %用FFT
                xx2=z1;
                vv2=fft(v2).^(-1);
                z2=vv2.*xx2;                
                x=x+ifft(z2);
                break
            end
      end
      if ( error <= tol )
          break
      end
      y = H(1:m,1:m) \ s(1:m);
      %x = x + V(:,1:m)*y;  
      %x = x + M\(V(:,1:m)*y);
      %x = x + M2\M1\(V(:,1:m)*y);
      v1=M1(:,1);   %用FFT
      xx1=fft((V(:,1:m)*y));
      vv1=fft(v1).^(-1);
      z1=vv1.*xx1;
 
      v2=M2(:,1);   %用FFT
      xx2=z1;
      vv2=fft(v2).^(-1);
      z2=vv2.*xx2;     
      x=x+ifft(z2);
      %r = M \ ( b-A*x );                              
      r =  b-A*x ;
      s(iter2+1) = norm(r);
      error = s(iter2+1) / bnrm2;                       
      if ( error <= tol )
          break
      end
   end
   time = toc;
   if ( error > tol )
       flag = 1;
   end               
   it=[iter iter2];
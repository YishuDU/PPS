%计算PPS,HSS最小谱半径以及对应理论最优参数并作图
%输出 理论最优参数alpha_PPS 最小谱半径rho_PPS 
%     理论最优参数alpha_HSS 最小谱半径rho_HSS
clear 
load('C:\Users\duyis\Desktop\PPS程序\example1_data\data9_1_5.mat')
P1_1=1/2*yita*M+Dplu_l*T_l-Dplu_r*T_r;
P2_1=1/2*yita*M+Dmin_r*T_l'-Dmin_l*T_r';
I = speye(N);
H = (A+A')/2;
S = (A-A')/2;
Q=zeros(150,3);
%% 计算PPS与HSS谱半径
for ii=1:1500
    alpha = 0.001*ii;
    Q(ii,1)=alpha;
    Q(ii,2)=vrho((alpha*I+S)^(-1)*(alpha*I-H)*(alpha*I+H)^(-1)*(alpha*I-S));   %HSS 
    Q(ii,3)=vrho((alpha*I+P2_1)^(-1)*(alpha*I-P1_1)*(alpha*I+P1_1)^(-1)*(alpha*I-P2_1));%PPS    
end
[Y,II]=min(Q,[],1);
alpha_PPS=Q(II(3),1)
rho_PPS=Y(3)
alpha_HSS=Q(II(2),1)
rho_HSS=Y(2)
%% 作图
plot(Q(:,1),Q(:,2),'-g',Q(:,1),Q(:,3),'-.r','LineWidth',1);
axis([0 1.5 0.1 1]); 
xlabel('\alpha');
ylabel('\rho(T(\alpha))');
title('N=512,\beta=0.9');
legend('\rho(HSS)','\rho(PPS)')

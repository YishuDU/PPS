%生成二维分数阶扩散方程系数矩阵与源项并保存数据
clear 
global yita M Dplus_l Dplus_r  Dminus_l Dminus_r T_l T_r I N
for i=3
N=2^i;  %空间方向N_x=N_y=2^i
M_t=N;
beta=0.5;%扩散项分数阶导数2-beta
h=(1-0)/(N+1);
deltat=(1-0)/M_t;
tao=1/(gamma(beta+1)*h^(1-beta));
yita=h/(tao*deltat);
t=1/M_t;
% 生成第一个时间层上的系数矩阵A
q=zeros(1,N);
for k=0:N
    if (k==0)
        q_0=(1/2)^beta;
    else if (k==1)
        q(1)=(3/2)^beta-2*(1/2)^beta;
    else 
        q(k)=(k+1/2)^beta-2*(k-1/2)^beta+(k-3/2)^beta;
        end
    end
end
c_l=[q_0,q(1:N-1)];
r_l=[q_0,zeros(1,N-1)];
T_l=toeplitz(c_l,r_l);
T_l=sparse(T_l);
c_r=q;
r_r=[q(1),q_0,zeros(1,N-2)];
T_r=toeplitz(c_r,r_r);
T_r=sparse(T_r);
e=ones(N^2,1);
M=spdiags([1/8*e,3/4*e,1/8*e],[-1,0,1],N,N);
x_1=h/2:h:1-3*h/2;
x_2=3*h/2:h:1-h/2;

dplus_l=[];
for i=1:N
    d=DDplus(x_1,i*h,t);
    dplus_l=[dplus_l,d];
end
Dplus_l=spdiags(dplus_l',0,N^2,N^2);

dplus_r=[];
for i=1:N
    d=DDplus(x_2,i*h,t);
    dplus_r=[dplus_r,d];
end
Dplus_r=spdiags(dplus_r',0,N^2,N^2);

dminus_l=[];
for i=1:N
    d=DDminus(x_1,i*h,t);
    dminus_l=[dminus_l,d];
end
Dminus_l=spdiags(dminus_l',0,N^2,N^2);

dminus_r=[];
for i=1:N
    d=DDminus(x_2,i*h,t);
    dminus_r=[dminus_r,d];
end
Dminus_r=spdiags(dminus_r',0,N^2,N^2);

I=speye(N);
A=yita*kron(M,M)+(Dplus_l*(kron(I,T_l)+kron(T_l,I))-Dplus_r*(kron(I,T_r)+kron(T_r,I)))+...
                     (Dminus_l*(kron(I,T_l')+kron(T_l',I))-Dminus_r*(kron(I,T_r')+kron(T_r',I)));
% 生成第一个时间层上的源项f
f=ones(N^2,1);
eval(['save data',num2str(i)])
end

function d=DDplus(x,y,t)
    d=0.01+t^2+0.01*exp(-8*x)+0.01*exp(-8*y);
end

function d=DDminus(x,y,t)
    d=0.01+t^2+0.01*exp(8*x)+0.01*exp(8*y);
end
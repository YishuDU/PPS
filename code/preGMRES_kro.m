clear
%针对二维问题
%预处理子 M_pps=1/(2*alpha)*(alpha*I+P1)*(alpha*I+P2)
%比较不同PPS,Strang Gilbert,Tony Chan预处理子PCG求解效率
%输入 系数矩阵A 右端项f 容忍误差tol=1e-5 最大迭代步数maxit=1000 预处理子M=M1*M2 初始向量x0=zeros(N,1)
%输出 三种预处理子对应的残差relres 迭代步数iter CPU时间time
load('example3_data\data32.mat')
dplus_l=mean(diag(Dplus_l));
dplus_r=mean(diag(Dplus_r));
dmins_r=mean(diag(Dminus_r));
dmins_l=mean(diag(Dminus_l));
%% PPS预处理子
alpha=4.33;%需调
P1_t=1/2*yita*kron(M,M)+(Dplus_l*(kron(I,T_l)+kron(T_l,I))-Dplus_r*(kron(I,T_r)+kron(T_r,I)));
P2_t=1/2*yita*kron(M,M)+(Dminus_l*(kron(I,T_l')+kron(T_l',I))-Dminus_r*(kron(I,T_r')+kron(T_r',I)));
II=eye(N^2);
P=(1/(2*alpha))*(alpha*II+P1_t)*(alpha*II+P2_t);
[~,iter1, time1, flag1, error1] = mygmres(A,zeros(N^2,1),f,P,N^2,1000,1e-5 )
% P1=sqrt((1/(2*alpha)))*(alpha*II+P1_t);
% P2=sqrt((1/(2*alpha)))*(alpha*II+P2_t);
% [~,iter, time, flag, error] = mygmres(A,zeros(N^2,1),f,P1,P2,N^2,1000,1e-5 )


%% Strang Gilbert预处理子
T_ls=preS(T_l);
T_rs=preS(T_r);
M_s=preS(M);

Ps=yita*kron(M_s,M_s)+(dplus_l*(kron(I,T_ls)+kron(T_ls,I))-dplus_r*(kron(I,T_rs)+kron(T_rs,I)))+...
                     (dminus_l*(kron(I,T_ls')+kron(T_ls',I))-dminus_r*(kron(I,T_rs')+kron(T_rs',I))); 
% Ps=yita*kron(M,M)+(Dplus_l*(kron(I,T_ls)+kron(T_ls,I))-Dplus_r*(kron(I,T_rs)+kron(T_rs,I)))+...
%                      (Dminus_l*(kron(I,T_ls')+kron(T_ls',I))-Dminus_r*(kron(I,T_rs')+kron(T_rs',I)));                 
[~,iter2, time2, flag2, error2] = mygmres(A,zeros(N^2,1),f,Ps,N^2,1000,1e-5 )

%% Tony Chan预处理子
T_lc=preC(T_l);
T_rc=preC(T_r);
M_c=preC(M);
Pc=yita*kron(M_c,M_c)+(dplus_l*(kron(I,T_lc)+kron(T_lc,I))-dplus_r*(kron(I,T_rc)+kron(T_rc,I)))+...
                     (dminus_l*(kron(I,T_lc')+kron(T_lc',I))-dminus_r*(kron(I,T_rc')+kron(T_rc',I)));  
[~,iter3, time3, flag3, error3] = mygmres(A,zeros(N^2,1),f,Pc,N^2,1000,1e-5 )

%% 无预处理子
[~,iter4, time4, flag4, error4] = mygmres(A,zeros(N^2,1),f,eye(N^2),N^2,1000,1e-5 )
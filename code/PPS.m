function [x,iter,time,res,flag] = PPS(A,alpha, x0, b, maxit, tol)
%采用PPS分裂 求解线性方程组Ax=b
%输入 系数矩阵A 位移参数alpha 初始向量x0 右端项b 最大迭代步数maxit 容忍误差tol
%输出 解x 迭代步数iter CPU时间time 残差res 收敛标志flag: flag=0表示在maxit次迭代内收敛至所需容差tol 
flag=0;
global yita M Dplu_l Dplu_r Dmin_l Dmin_r T_l T_r N
P1_1=1/2*yita*M+Dplu_l*T_l-Dplu_r*T_r;
P2_1=1/2*yita*M+Dmin_r*T_l'-Dmin_l*T_r';

P = alpha*speye(N,N);
IH = inv(P+P1_1);
IS = inv(P+P2_1);

b1 = IH*b;
b2 = IS*b;
x = x0;
bb=norm(b-A*x0);
t0 = clock;
for iter=1:maxit
    x = IH*(alpha*x-P2_1*x)+b1;
    x = IS*(alpha*x-P1_1*x)+b2;
    res=norm(b-A*x)/bb;
    if ( res < tol)
        flag = 1;
        break;
    end
end
time = etime(clock,t0);